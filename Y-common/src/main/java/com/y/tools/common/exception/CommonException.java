package com.y.tools.common.exception;


/**
 * Created by YYF
 *
 * Date: 2017/7/24
 *
 * Project_name :Y-Tools
 */
public class CommonException extends RuntimeException {

    public static final CommonException USER_EXCEPTION = new CommonException(100001, "user is not exsit");

    private final int code;

    protected CommonException(int code, String message) {
        super(message);
        this.code = code;
    }

    public CommonException setMessage(String message) {
        return new CommonException(code, message);
    }

    public int getCode() {
        return code;
    }
}
