package com.y.tools.common.utils;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 日期基础方法
 *
 *
 * Date: 2017/4/24
 *
 * Project_name :Y-Tools
 */
public class DateUtils {

    public static final String SHOT_DATE = "yy-MM-dd";
    public static final String NORMAL_DATE = "yyyy-MM-dd";
    public static final String SHOT_TIME = "yyyy-MM-dd HH:mm:ss";
    public static final String LONG_TIME = "yyyy-MM-dd HH:mm:ss sss";

    private static final SimpleDateFormat SHOT_DATE_SDF = new SimpleDateFormat(SHOT_DATE);
    private static final SimpleDateFormat normalDateSDF = new SimpleDateFormat(NORMAL_DATE);
    private static final SimpleDateFormat shotTimeSDF = new SimpleDateFormat(SHOT_TIME);
    private static final SimpleDateFormat longTimeSDF = new SimpleDateFormat(LONG_TIME);


    public static String dateToShortDate(Date date) {
        return SHOT_DATE_SDF.format(date);
    }

    public static String dateToNormalDate(Date date) {
        return normalDateSDF.format(date);
    }

    public static String dateToShortTime(Date date) {
        return shotTimeSDF.format(date);
    }

    public static String dateToLongTime(Date date) {
        return longTimeSDF.format(date);
    }

    public static Date shortDateToDate(String shortDate) throws ParseException {
        Date date = SHOT_DATE_SDF.parse(shortDate);
        return date;
    }

    public static Date nomalDateToDate(String normalDate) throws ParseException {
        Date date = normalDateSDF.parse(normalDate);
        return date;
    }

    public static Date shortTimeToDate(String shortTime) throws ParseException {
        Date date = shotTimeSDF.parse(shortTime);
        return date;
    }

    public static Date longTimeToDate(String longTime) throws ParseException {
        Date date = longTimeSDF.parse(longTime);
        return date;
    }

    /**
     * 得到本月最后一天的日期
     *
     * @return Date
     * @Methods Name getLastDayOfMonth
     */
    public static long getLastDayOfMonth() {
        Calendar cDay = Calendar.getInstance();
        cDay.set(Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH),
                23,
                59,
                59
        );
        cDay.set(Calendar.DAY_OF_MONTH, cDay.getActualMaximum(Calendar.DAY_OF_MONTH));
        return cDay.getTimeInMillis();
    }

    /**
     * 计算本月剩余秒数, 月末-当前
     *
     * @return
     */
    public static long calcCurrMonthSurplusSeconds() {
        return (getLastDayOfMonth() - System.currentTimeMillis()) / 1000;
    }
}
