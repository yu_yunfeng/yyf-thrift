package com.y.tools.common;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Jooq 连接
 *
 * Date: 2017/7/24
 *
 * Project_name :yyf-thrif
 */
public enum DBConnHelper {

    DBConn;

    private BoneCP connectionPool;

    private BoneCP initConnectionPool() {
        BoneCP connectionPool = null;
        try {
            // register jdbc driver
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            // read configs from properties reader
            String url = "jdbc:mysql://db1.dqprism.com:3306";
            String userName = "daqi";
            String password = "5F51692091B4031640E18E7C27430E071BC878C8";
            Integer minConnections = 5;
            Integer maxConnections = 10;
            Integer idleMaxAgeInMinutes = 10;
            Integer acquireRetryDelayInMs = 3000;
            Integer acquireRetryAttempts = 3;
            // init connection pool
            BoneCPConfig config = new BoneCPConfig();
            config.setJdbcUrl(url);
            config.setUsername(userName);
            config.setPassword(password);
            config.setMinConnectionsPerPartition(minConnections);
            config.setMaxConnectionsPerPartition(maxConnections);
            config.setIdleMaxAgeInMinutes(idleMaxAgeInMinutes);
            config.setAcquireRetryDelayInMs(acquireRetryDelayInMs);
            config.setAcquireRetryAttempts(acquireRetryAttempts);
            connectionPool = new BoneCP(config);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
        }
        return connectionPool;
    }

    public DSLContext getJooqDSL(Connection conn) throws SQLException {
        return DSL.using(conn, SQLDialect.MYSQL);
    }

    public Connection getConn() throws SQLException {
        if (connectionPool == null) {
            connectionPool = initConnectionPool();
        }
        return connectionPool.getConnection();
    }
}
