package com.y.tools.common.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by YYF
 *
 * Date: 2017/7/20
 *
 * Project_name :Y-Tools
 */
public enum ChannelType {

    JOB51(1, "51job", "51job"),
    LIEPIN(2, "liepin", "猎聘"),
    ZHILIAN(3, "zhaopin", "智联"),
    LINKEDIN(4, "linkedin", "领英"),
    ALIPAY(5, "alipay", "支付宝");

    ChannelType(int value, String name, String alias) {
        this.value = value;
        this.name = name;
        this.alias = alias;
    }

    private int value = 0;                //渠道值
    private String name = null;            //渠道名称
    private String alias = null;        //渠道别名


    private static final String BINDING = "position/binding"; //帐号绑定请求名称
    private static final String REMAIN_NUM = "position/remain"; //帐号绑定请求名称

    private static final Map<Integer, ChannelType> intToEnum
            = new HashMap<Integer, ChannelType>();

    static {
        for (ChannelType op : values())
            intToEnum.put(op.getValue(), op);
    }

    public static ChannelType instaceFromInteger(int value) {
        return intToEnum.get(value);
    }

    public int getValue() {
        return this.value;
    }

    /**
     * 返回该渠道的绑定请求地址
     *
     * @param domain
     * @param domain chaos域名
     * @return
     */
    public String getBindURI(String domain) {
        return domain + "/" + name + "/" + BINDING;
    }

    /**
     * 返回该渠道的绑定请求地址
     *
     * @param domain chaos域名
     * @return
     */
    public String getRemain(String domain) {
        return domain + "/" + name + "/" + REMAIN_NUM;
    }

    /**
     * 返回该渠道的可发布职位数的请求地址
     *
     * @param domain chaos域名
     * @return
     */
    public String getRemainURI(String domain) {
        return domain + "/" + name + "/" + REMAIN_NUM;
    }

    public String getAlias() {
        return alias;
    }
}

