package com.y.tools.common;

import com.jolbox.bonecp.BoneCP;
import com.jolbox.bonecp.BoneCPConfig;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * BoneCP连接池
 *
 * Date: 2017/7/24
 *
 * Project_name :yyf-thrif
 */
public class DatabaseConnectionHelper {

    private static BoneCP connectionPool;
    private static DatabaseConnectionHelper self;

    private BoneCP initConnectionPool() {
        BoneCP connectionPool = null;
        try {
            // register com.y.tools.basic.jdbc driver
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            // read configs from properties reader
            String url = "com.y.tools.basic.jdbc:mysql://db1.dqprism.com:3306/";
            String userName = "daqi";
            String password = "5F51692091B4031640E18E7C27430E071BC878C8";
            Integer minConnections = 5;
            Integer maxConnections = 10;
            // init connection pool
            BoneCPConfig config = new BoneCPConfig();
            config.setJdbcUrl(url);
            config.setUsername(userName);
            config.setPassword(password);
            config.setMinConnectionsPerPartition(minConnections);
            config.setMaxConnectionsPerPartition(maxConnections);
            connectionPool = new BoneCP(config);
        } catch (Exception e) {
            // send notification
            e.printStackTrace();
        }
        return connectionPool;
    }

    public static DatabaseConnectionHelper getConnection() {
        if (self == null) {
            self = new DatabaseConnectionHelper();
        }
        return self;
    }

    private DatabaseConnectionHelper() {
        connectionPool = initConnectionPool();
    }

    public DSLContext getJooqDSL() throws SQLException {
        Connection conn = connectionPool.getConnection();
        return DSL.using(conn, SQLDialect.MYSQL);
    }

}
