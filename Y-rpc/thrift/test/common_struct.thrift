# file: com.y.tools.common.struct

namespace java com.yuyunfeng.com.y.tools.rpc.thrift

struct Response { 
    1: i32 status,
    2: string message,
    3: optional string data
}


struct CommonQuery {
    1: i32 appid,
    2: optional i32 page,
    3: optional i32 per_page,
    4: optional string sortby,
    5: optional string order,
    6: optional string fields,
    7: optional bool nocache=false,
    8: optional map<string, string> equalFilter,
    9: optional list<string> attributes,
    10: optional list<string> grouops
}


typedef string Timestamp;

/*
 * 个人中心推荐记录
 */
struct ApplicationRecordsForm {
    1: optional i32 id,
    2: optional string position_title,
    3: optional string company_name,
    4: optional string status_name,
    5: optional Timestamp time
}



struct Xtruct
{
  1:  string string_thing,
  4:  i8     byte_thing,
  9:  i32    i32_thing,
  11: i64    i64_thing
}