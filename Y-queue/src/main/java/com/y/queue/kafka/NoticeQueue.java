package com.y.queue.kafka;


import kafka.producer.KeyedMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;


/**
 * Created by YYF
 *
 * Date: 2017/11/10
 *
 * Project_name :Y-Project
 */
public class NoticeQueue {

    // 100W 预估最大能占到200-300M内存
    private final static ArrayBlockingQueue<KeyedMessage<String, String>> queue = new ArrayBlockingQueue<KeyedMessage<String, String>>(1000000);
    //    private final static Logger logserverLog = Log.getLogger("appstore_logserver");
    private static boolean flush2Kafka = true;
    private static int NextSleepTime = 5;
    public static String ALARM_URL = "http://119.147.212.244:9199/de/reportAlarm";

    public static void start() {
        new Thread(flushData2Kafka).start();
    }

//    public static void offer(Msg notice) {
//        String topic = "";
//        if (notice instanceof ActMsg) {
//            topic = KafkaProducerAgent.actTopic;
//        } else if (notice instanceof ClickMsg) {
//            topic = KafkaProducerAgent.clickTopic;
//        } else if (notice instanceof DownloadMsg) {
//            topic = KafkaProducerAgent.downloadTopic;
//        } else if (notice instanceof ShowMsg) {
//            topic = KafkaProducerAgent.showTopic;
//        } else if (notice instanceof DebugLogMsg) {
//            topic = KafkaProducerAgent.debugLogTopic;
////		} else if(notice instanceof IntegrationMsg){
////			//老版本集成测试topic
////			topic = KafkaProducerAgent.IntegrationTopic;
//        } else if (notice instanceof IntegrationBaseMsg) {
//            //新版本集成测试topic
//            topic = KafkaProducerAgent.IntegrationTopic;
//        } else if (notice instanceof OnlineMsg) {
//            //老颁布实时计算topic
//            topic = KafkaProducerAgent.onlineTopic;
//        } else if (notice instanceof OnlineBaseMsg) {
//            //新颁布实时计算topic
//            topic = KafkaProducerAgent.OnlineTopicV1;
//        }
//        if (!queue.offer(new KeyedMessage<String, String>(topic, null, notice.toString()))) {
////            // 队列满，非常危险
////            logserverLog.error("notice queue full!!!");
////            String parameter = "title=【LogServer队列满了】"
////                    + "&type=mysql&content=【LogServer队列满了】&others=pingxiaozhang@126.com,seonzhang@digitcube.net,yaolihong@dataeye.com";
////            SendUtil.SendToUrlByParameter(ALARM_URL, parameter);
//        }
//    }

    private final static Runnable flushData2Kafka = new Runnable() {

        public void run() {
            while (flush2Kafka) {
                try {
                    List<KeyedMessage<String, String>> noticeList = new ArrayList<KeyedMessage<String, String>>(2000);
                    queue.drainTo(noticeList);
                    // 下一次睡眠时间
                    NextSleepTime = getSleepTime(noticeList.size());
                    // 记录日志
//                    logserverLog.info("NoticeQueue queue size="
//                            + noticeList.size() + ", NextSleepTime="
//                            + NextSleepTime + "s");
                    KafkaProducerAgent.send(noticeList);
                } catch (Throwable t) {
//                    logserverLog.error("flush oss data 2 kafka error", t);
                }
                try {
                    Thread.sleep(NextSleepTime * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public static int getSleepTime(int queueSize) {
        if (queueSize < 2000)
            return 5;
        if (queueSize < 10000)
            return 3;
        return 1;
    }
}
