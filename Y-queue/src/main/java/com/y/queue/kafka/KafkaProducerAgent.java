package com.y.queue.kafka;

import java.util.List;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

public class KafkaProducerAgent {

    private static Producer<String, String> producer = null;
    public static String OnlineTopicV1 = "dcGa20OnlineTopicV1";
    public static String onlineTopic = "dcGa20OnlineTopic";
    public static String actTopic = "dcGa20AppStoreActTopic";
    public static String clickTopic = "dcGa20AppStoreClickTopic";
    public static String downloadTopic = "dcGa20AppStoreDownloadTopic";
    public static String showTopic = "dcGa20AppStoreShowTopic";
    public static String debugLogTopic = "dcGa20AppStoreDebugLogTopic";
    public static String IntegrationTopic = "dcGa20IntegrationTopic";

    static {
        init();
    }

    private KafkaProducerAgent() {
    }

    private static void init() {
        ProducerConfig config = new ProducerConfig(ProfileManager.get());
        producer = new Producer<String, String>(config);
    }

    public static void send(List<KeyedMessage<String, String>> list) {
        producer.send(list);
    }
}
