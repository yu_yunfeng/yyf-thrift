package com.y.extra.dbdoc.obj;

import java.util.List;

/**
 * Created by YYF
 *
 * Date: 2017/4/24
 *
 * Project_name :Y-Tools
 */
public class DataBase {
    // 数据库名
    private String dbName;
    // 表的列表
    private List<Table> tables;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public List<Table> getTables() {
        return tables;
    }

    public void setTables(List<Table> tables) {
        this.tables = tables;
    }
}
