package com.y.extra.dbdoc;

import com.y.extra.dbdoc.obj.DataBase;
import com.y.extra.dbdoc.obj.Table;
import com.y.extra.dbdoc.obj.TableDetails;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by YYF
 *
 * 通过数据库连接将表生成MarkDown文档
 *
 * Date: 2017/4/21
 *
 * Project_name :Y-Tools
 */
public class TableComment {

    /**
     * 数据库连接
     *
     * @return Connection
     * @throws Exception
     */
    public static Connection getMySQLConnection() throws Exception {
        Class.forName("com.mysql.jdbc.Driver");
        Connection conn = DriverManager.getConnection("jdbc:mysql://111.231.153.252", "root", "sdfsdfsd*%$123");
        return conn;
    }


    /**
     * 获取当前数据库下的所有表名称
     *
     * @param dbName
     * @return List
     * @throws Exception
     */
    public static List getAllTableName(String dbName) throws Exception {
        List tables = new ArrayList();
        Connection conn = getMySQLConnection();
        Statement stmt = conn.createStatement();
        ResultSet rs = stmt.executeQuery("SHOW TABLES FROM " + dbName);
        while (rs.next()) {
            String tableName = rs.getString(1);
            tables.add(tableName);
        }
        rs.close();
        stmt.close();
        conn.close();
        return tables;
    }


    /**
     * 获得某表的建表语句
     *
     * @param tableName
     * @param db
     * @return Map
     * @throws Exception
     */
    public static Map<String, Table> getCommentByTableName(List tableName, String db) throws Exception {
        Map map = new HashMap();
        Connection conn = getMySQLConnection();
        Statement stmt = conn.createStatement();
        for (int i = 0; i < tableName.size(); i++) {
            String table = (String) tableName.get(i);
            System.out.println("SHOW CREATE TABLE " + db + "." + table);
            ResultSet rs = null;
            if (table.indexOf(".") > -1) {
                rs = stmt.executeQuery("SHOW CREATE TABLE " + table);
            } else {
                rs = stmt.executeQuery("SHOW CREATE TABLE " + db + "." + table);
            }
            if (rs != null && rs.next()) {
                Table tableTemp = new Table();
                String createDDL = rs.getString(2);
                tableTemp.setTableName(table);
                tableTemp.setTableComment(parse(createDDL));
                tableTemp.setSql(createDDL);
                map.put(table, tableTemp);
            }
            rs.close();
        }
        stmt.close();
        conn.close();
        return map;
    }


    /**
     * 获得某表中所有字段的注释
     *
     * @param tableslist
     * @param db
     * @return List
     * @throws Exception
     */
    public static List<Table> getColumnCommentByTableName(List<Table> tableslist, String db) throws Exception {
        Connection conn = getMySQLConnection();
        Statement stmt = conn.createStatement();
        for (Table table : tableslist) {
            // 所有表
            List<TableDetails> list = new ArrayList();

            ResultSet rs = null;
            if (table.getTableName().indexOf(".") > -1) {
                rs = stmt.executeQuery("show full columns from " + table.getTableName());
            } else {
                rs = stmt.executeQuery("show full columns from " + db + "." + table.getTableName());
            }
            // 表名
            while (rs.next()) {
                TableDetails tableDetails = new TableDetails();
                // 字段名
                tableDetails.setFieldName(rs.getString("Field") != null ? rs.getString("Field") : "");
                // 字段类型
                tableDetails.setFieldType(rs.getString("Type") != null ? rs.getString("Type") : "");
                // 字段说明
                tableDetails.setFieldComment(rs.getString("Comment") != null ? rs.getString("Comment") : "");
                list.add(tableDetails);
                table.setTableDetailsList(list);
            }
            rs.close();
        }
        stmt.close();
        conn.close();
        return tableslist;
    }


    /**
     * 返回注释信息(解析注释信息)
     *
     * @param all
     * @return String
     */
    public static String parse(String all) {
        String comment = null;
        int index = all.indexOf("COMMENT='");
        if (index < 0) {
            return "";
        }
        comment = all.substring(index + 9);
        comment = comment.substring(0, comment.length() - 1);
        return comment;
    }

    /**
     * 循环数据库中的表
     *
     * @return
     * @throws Exception
     */
    public static List<DataBase> DataBases() throws Exception {
        List<DataBase> list = new ArrayList();
//        String[] strings = new String[]{"analytics", "campaigndb", "candidatedb", "configdb", "dictdb", "historydb", "hrdb", "jobdb", "userdb", "logdb", "profiledb"};
        String[] strings = new String[]{"travel"};
        for (String db : strings) {
            DataBase dataBase = new DataBase();
            dataBase.setDbName(db);
            // 获取数据所有的表名
            List tables = getAllTableName(db);
            Map tablesComment = getCommentByTableName(tables, db);
            Set names = tablesComment.keySet();
            Iterator iter = names.iterator();
            List<Table> tableList = new ArrayList();
            while (iter.hasNext()) {
                String name = (String) iter.next();
                tableList.add((Table) tablesComment.get(name));
            }
            List<Table> tablesDetails = getColumnCommentByTableName(tableList, db);
            dataBase.setTables(tablesDetails);
            list.add(dataBase);
        }
        return list;
    }


    /**
     * 写入文件
     *
     * @param filepath
     * @param list
     * @return
     * @throws IOException
     */
    public static boolean writeFile(String filepath, List<DataBase> list) throws IOException {
        Boolean bool = false;
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            File file = new File(filepath);//文件路径(包括文件名称)
            //将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();

            int i = 1;
            buffer.append("# 数据库设计");
            buffer.append(System.getProperty("line.separator"));
            buffer.append("[toc]");
            buffer.append(System.getProperty("line.separator"));
            for (DataBase dataBase : list) {
                buffer.append("## " + i + "." + dataBase.getDbName() + "库");
                int j = 1;
                for (Table table : dataBase.getTables()) {
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("### ");
                    buffer.append(i);
                    buffer.append(".");
                    buffer.append(j);
                    buffer.append(" " + table.getTableComment());
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 表名：");
                    buffer.append(table.getTableName());
                    buffer = buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 说明：");
                    buffer.append(table.getTableComment());
                    buffer = buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 字段描述：");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("| 字段名称 | 类型 | 说明 | 备注 |");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("|-------|-------|-------|-------|");
                    buffer.append(System.getProperty("line.separator"));
                    for (TableDetails tableDetails : table.getTableDetailsList()) {
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldName());
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldType());
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldComment());
                        buffer.append("|");
                        buffer.append("");
                        buffer.append("|");
                        buffer.append(System.getProperty("line.separator"));
                    }
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 建表语句：");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("```sql");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(table.getSql());
                    buffer = buffer.append(System.getProperty("line.separator"));
                    buffer.append("```");
                    buffer = buffer.append(System.getProperty("line.separator"));
                    j = j + 1;
                }
                i = i + 1;
            }
            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buffer.toString().toCharArray());
            pw.flush();
            bool = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // 关闭数据库连接
            pw.close();
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return bool;
    }


    /**
     * 写入文件
     *
     * @param filepath
     * @param dataBase
     * @param menu
     * @return
     * @throws IOException
     */
    public static boolean writeFileContent(String filepath, DataBase dataBase, Integer menu) throws IOException {
        Boolean bool = false;
        String temp = "";

        FileInputStream fis = null;
        InputStreamReader isr = null;
        BufferedReader br = null;
        FileOutputStream fos = null;
        PrintWriter pw = null;
        try {
            File file = new File(filepath);//文件路径(包括文件名称)
            //将文件读入输入流
            fis = new FileInputStream(file);
            isr = new InputStreamReader(fis);
            br = new BufferedReader(isr);
            StringBuffer buffer = new StringBuffer();

            int i = 1;
            if (dataBase != null) {
                buffer.append("## " + menu + "." + dataBase.getDbName() + "库");
                buffer.append(System.getProperty("line.separator"));
                buffer.append("[toc]");
                for (Table table : dataBase.getTables()) {
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("### ");
                    buffer.append(menu);
                    buffer.append(".");
                    buffer.append(i);
                    buffer.append(" ");
                    buffer.append(table.getTableName());

                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 说明：");
                    buffer.append(table.getTableComment());
                    buffer = buffer.append(System.getProperty("line.separator"));
                    buffer = buffer.append(System.getProperty("line.separator"));


                    buffer.append("- 字段描述：");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("| 字段名称 | 类型 | 说明 | 备注 |");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("|-------|-------|-------|-------|");
                    buffer.append(System.getProperty("line.separator"));
                    for (TableDetails tableDetails : table.getTableDetailsList()) {
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldName());
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldType());
                        buffer.append("|");
                        buffer.append(tableDetails.getFieldComment());
                        buffer.append("|");
                        buffer.append("|");
                        buffer.append(System.getProperty("line.separator"));
                    }

                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("- 建表语句：");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append("```sql");
                    buffer.append(System.getProperty("line.separator"));
                    buffer.append(table.getSql());
                    buffer = buffer.append(System.getProperty("line.separator"));
                    buffer.append("```");
                    buffer = buffer.append(System.getProperty("line.separator"));


                    i = i + 1;

                }

            }
            fos = new FileOutputStream(file);
            pw = new PrintWriter(fos);
            pw.write(buffer.toString().toCharArray());
            pw.flush();
            bool = true;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            //不要忘记关闭
            if (pw != null) {
                pw.close();
            }
            if (fos != null) {
                fos.close();
            }
            if (br != null) {
                br.close();
            }
            if (isr != null) {
                isr.close();
            }
            if (fis != null) {
                fis.close();
            }
        }
        return bool;
    }


    /**
     * main 方法
     *
     * @param args
     * @throws Exception
     */
    public static void main(String args[]) throws Exception {
        List<DataBase> list = TableComment.DataBases();
        StringBuffer stringBuffer = null;
        String path = "/Users/yuyunfeng/Desktop/非凡/";
        int i = 1;
        try {
//            for (DataBase dataBase : list) {
//                stringBuffer = new StringBuffer();
//                stringBuffer.append(path);
//                stringBuffer.append(dataBase.getDbName());
//                stringBuffer.append(".md");
//                File file = new File(stringBuffer.toString());
//                if (!file.exists()) {
//                    file.createNewFile();
//                }
//                // 开始写文件
//                writeFileContent(stringBuffer.toString(), dataBase, i);
//                i = i + 1;
//            }

            stringBuffer = new StringBuffer();
            stringBuffer.append(path);
            stringBuffer.append("db.md");
            File file = new File(stringBuffer.toString());
            if (!file.exists()) {
                file.createNewFile();
            }
            writeFile(stringBuffer.toString(), list);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}