package com.y.extra.dbdoc.obj;

import java.util.List;

/**
 * Created by YYF
 *
 * Date: 2017/4/24
 *
 * Project_name :Y-Tools
 */
public class Table {
    // 表名
    private String tableName;
    // 表说明
    private String tableComment;
    // SQL 语气
    private String sql;
    // 表详情
    private List<TableDetails> tableDetailsList;

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getTableComment() {
        return tableComment;
    }

    public void setTableComment(String tableComment) {
        this.tableComment = tableComment;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<TableDetails> getTableDetailsList() {
        return tableDetailsList;
    }

    public void setTableDetailsList(List<TableDetails> tableDetailsList) {
        this.tableDetailsList = tableDetailsList;
    }
}
