package com.y.extra.dbdoc.obj;

import com.github.crab2died.annotation.ExcelField;

/**
 * Created by YYF
 *
 * Date: 2017/4/24
 *
 * Project_name :Y-Tools
 */
public class TableDetails {
    // 字段名
    @ExcelField(title = "字段名称", order = 1)
    private String fieldName;
    // 字段类型
    @ExcelField(title = "类型", order = 2)
    private String fieldType;
    // 字段说明
    @ExcelField(title = "说明", order = 3)
    private String fieldComment;

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public String getFieldType() {
        return fieldType;
    }

    public void setFieldType(String fieldType) {
        this.fieldType = fieldType;
    }

    public String getFieldComment() {
        return fieldComment;
    }

    public void setFieldComment(String fieldComment) {
        this.fieldComment = fieldComment;
    }
}
