package com.y.extra.excel;


import com.github.crab2died.ExcelUtils;
import com.y.extra.dbdoc.TableComment;
import com.y.extra.dbdoc.obj.DataBase;
import com.y.extra.dbdoc.obj.Table;
import com.y.extra.dbdoc.obj.TableDetails;

import java.util.List;

/**
 * Created by YYF
 *
 * Excel文件操作
 *
 * Date: 2017/4/24
 *
 * Project_name :Y-Tools
 */
public class ExcelTools {


    public static void main(String[] args) throws Exception {

        String excelPath = "/Users/yuyunfeng/Desktop/data.xlsx";

        List<DataBase> list = TableComment.DataBases();
        for (DataBase dataBase : list) {
            for (Table table : dataBase.getTables()) {
                ExcelUtils.getInstance().exportObjects2Excel(table.getTableDetailsList(), TableDetails.class, true, table.getTableName(), true, excelPath);
            }
        }
    }


}
