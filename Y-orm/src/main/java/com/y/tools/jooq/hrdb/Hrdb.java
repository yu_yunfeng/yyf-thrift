/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.hrdb;


import com.y.tools.jooq.hrdb.tables.HrAppCvConf;
import com.y.tools.jooq.hrdb.tables.HrChatUnreadCount;
import com.y.tools.jooq.hrdb.tables.HrChildCompany;
import com.y.tools.jooq.hrdb.tables.HrCmsMedia;
import com.y.tools.jooq.hrdb.tables.HrCmsModule;
import com.y.tools.jooq.hrdb.tables.HrCmsPages;
import com.y.tools.jooq.hrdb.tables.HrCompany;
import com.y.tools.jooq.hrdb.tables.HrCompanyAccount;
import com.y.tools.jooq.hrdb.tables.HrCompanyConf;
import com.y.tools.jooq.hrdb.tables.HrEmployeeCertConf;
import com.y.tools.jooq.hrdb.tables.HrEmployeeCustomFields;
import com.y.tools.jooq.hrdb.tables.HrEmployeePosition;
import com.y.tools.jooq.hrdb.tables.HrEmployeeSection;
import com.y.tools.jooq.hrdb.tables.HrFeedback;
import com.y.tools.jooq.hrdb.tables.HrHbConfig;
import com.y.tools.jooq.hrdb.tables.HrHbItems;
import com.y.tools.jooq.hrdb.tables.HrHbPositionBinding;
import com.y.tools.jooq.hrdb.tables.HrHbScratchCard;
import com.y.tools.jooq.hrdb.tables.HrHbSendRecord;
import com.y.tools.jooq.hrdb.tables.HrHtml5Statistics;
import com.y.tools.jooq.hrdb.tables.HrHtml5UniqueStatistics;
import com.y.tools.jooq.hrdb.tables.HrImporterMonitor;
import com.y.tools.jooq.hrdb.tables.HrMedia;
import com.y.tools.jooq.hrdb.tables.HrOperationRecord;
import com.y.tools.jooq.hrdb.tables.HrPointsConf;
import com.y.tools.jooq.hrdb.tables.HrRecruitStatistics;
import com.y.tools.jooq.hrdb.tables.HrRecruitUniqueStatistics;
import com.y.tools.jooq.hrdb.tables.HrReferralStatistics;
import com.y.tools.jooq.hrdb.tables.HrResource;
import com.y.tools.jooq.hrdb.tables.HrRuleStatistics;
import com.y.tools.jooq.hrdb.tables.HrRuleUniqueStatistics;
import com.y.tools.jooq.hrdb.tables.HrSearchCondition;
import com.y.tools.jooq.hrdb.tables.HrSuperaccountApply;
import com.y.tools.jooq.hrdb.tables.HrTalentpool;
import com.y.tools.jooq.hrdb.tables.HrTeam;
import com.y.tools.jooq.hrdb.tables.HrTeamBluefocus;
import com.y.tools.jooq.hrdb.tables.HrTeamMember;
import com.y.tools.jooq.hrdb.tables.HrThirdPartyAccount;
import com.y.tools.jooq.hrdb.tables.HrThirdPartyPosition;
import com.y.tools.jooq.hrdb.tables.HrTopic;
import com.y.tools.jooq.hrdb.tables.HrWxBasicReply;
import com.y.tools.jooq.hrdb.tables.HrWxHrChat;
import com.y.tools.jooq.hrdb.tables.HrWxHrChatList;
import com.y.tools.jooq.hrdb.tables.HrWxImageReply;
import com.y.tools.jooq.hrdb.tables.HrWxModule;
import com.y.tools.jooq.hrdb.tables.HrWxNewsReply;
import com.y.tools.jooq.hrdb.tables.HrWxNoticeMessage;
import com.y.tools.jooq.hrdb.tables.HrWxRule;
import com.y.tools.jooq.hrdb.tables.HrWxTemplateMessage;
import com.y.tools.jooq.hrdb.tables.HrWxWechat;
import com.y.tools.jooq.hrdb.tables.HrWxWechatNoticeSyncStatus;
import com.y.tools.jooq.hrdb.tables.VPositionRealcompany;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class Hrdb extends SchemaImpl {

    private static final long serialVersionUID = -813469175;

    /**
     * The reference instance of <code>hrdb</code>
     */
    public static final Hrdb HRDB = new Hrdb();

    /**
     * No further instances allowed
     */
    private Hrdb() {
        super("hrdb");
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
                HrAppCvConf.HR_APP_CV_CONF,
                HrChatUnreadCount.HR_CHAT_UNREAD_COUNT,
                HrChildCompany.HR_CHILD_COMPANY,
                HrCmsMedia.HR_CMS_MEDIA,
                HrCmsModule.HR_CMS_MODULE,
                HrCmsPages.HR_CMS_PAGES,
                HrCompany.HR_COMPANY,
                HrCompanyAccount.HR_COMPANY_ACCOUNT,
                HrCompanyConf.HR_COMPANY_CONF,
                HrEmployeeCertConf.HR_EMPLOYEE_CERT_CONF,
                HrEmployeeCustomFields.HR_EMPLOYEE_CUSTOM_FIELDS,
                HrEmployeePosition.HR_EMPLOYEE_POSITION,
                HrEmployeeSection.HR_EMPLOYEE_SECTION,
                HrFeedback.HR_FEEDBACK,
                HrHbConfig.HR_HB_CONFIG,
                HrHbItems.HR_HB_ITEMS,
                HrHbPositionBinding.HR_HB_POSITION_BINDING,
                HrHbScratchCard.HR_HB_SCRATCH_CARD,
                HrHbSendRecord.HR_HB_SEND_RECORD,
                HrHtml5Statistics.HR_HTML5_STATISTICS,
                HrHtml5UniqueStatistics.HR_HTML5_UNIQUE_STATISTICS,
                HrImporterMonitor.HR_IMPORTER_MONITOR,
                HrMedia.HR_MEDIA,
                HrOperationRecord.HR_OPERATION_RECORD,
                HrPointsConf.HR_POINTS_CONF,
                HrRecruitStatistics.HR_RECRUIT_STATISTICS,
                HrRecruitUniqueStatistics.HR_RECRUIT_UNIQUE_STATISTICS,
                HrReferralStatistics.HR_REFERRAL_STATISTICS,
                HrResource.HR_RESOURCE,
                HrRuleStatistics.HR_RULE_STATISTICS,
                HrRuleUniqueStatistics.HR_RULE_UNIQUE_STATISTICS,
                HrSearchCondition.HR_SEARCH_CONDITION,
                HrSuperaccountApply.HR_SUPERACCOUNT_APPLY,
                HrTalentpool.HR_TALENTPOOL,
                HrTeam.HR_TEAM,
                HrTeamBluefocus.HR_TEAM_BLUEFOCUS,
                HrTeamMember.HR_TEAM_MEMBER,
                HrThirdPartyAccount.HR_THIRD_PARTY_ACCOUNT,
                HrThirdPartyPosition.HR_THIRD_PARTY_POSITION,
                HrTopic.HR_TOPIC,
                HrWxBasicReply.HR_WX_BASIC_REPLY,
                HrWxHrChat.HR_WX_HR_CHAT,
                HrWxHrChatList.HR_WX_HR_CHAT_LIST,
                HrWxImageReply.HR_WX_IMAGE_REPLY,
                HrWxModule.HR_WX_MODULE,
                HrWxNewsReply.HR_WX_NEWS_REPLY,
                HrWxNoticeMessage.HR_WX_NOTICE_MESSAGE,
                HrWxRule.HR_WX_RULE,
                HrWxTemplateMessage.HR_WX_TEMPLATE_MESSAGE,
                HrWxWechat.HR_WX_WECHAT,
                HrWxWechatNoticeSyncStatus.HR_WX_WECHAT_NOTICE_SYNC_STATUS,
                VPositionRealcompany.V_POSITION_REALCOMPANY);
    }
}
