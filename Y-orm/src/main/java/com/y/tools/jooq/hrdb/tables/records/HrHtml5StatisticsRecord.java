/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.hrdb.tables.records;


import com.y.tools.jooq.hrdb.tables.HrHtml5Statistics;

import java.sql.Date;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record6;
import org.jooq.Row6;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * 专题传播统计次数表
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class HrHtml5StatisticsRecord extends UpdatableRecordImpl<HrHtml5StatisticsRecord> implements Record6<Integer, Integer, Integer, Integer, Integer, Date> {

    private static final long serialVersionUID = 1953853979;

    /**
     * Setter for <code>hrdb.hr_html5_statistics.id</code>. primary key
     */
    public void setId(Integer value) {
        setValue(0, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.id</code>. primary key
     */
    public Integer getId() {
        return (Integer) getValue(0);
    }

    /**
     * Setter for <code>hrdb.hr_html5_statistics.topic_id</code>. wx_topic.id
     */
    public void setTopicId(Integer value) {
        setValue(1, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.topic_id</code>. wx_topic.id
     */
    public Integer getTopicId() {
        return (Integer) getValue(1);
    }

    /**
     * Setter for <code>hrdb.hr_html5_statistics.company_id</code>. company.id
     */
    public void setCompanyId(Integer value) {
        setValue(2, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.company_id</code>. company.id
     */
    public Integer getCompanyId() {
        return (Integer) getValue(2);
    }

    /**
     * Setter for <code>hrdb.hr_html5_statistics.view_num_pv</code>. 浏览次数
     */
    public void setViewNumPv(Integer value) {
        setValue(3, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.view_num_pv</code>. 浏览次数
     */
    public Integer getViewNumPv() {
        return (Integer) getValue(3);
    }

    /**
     * Setter for <code>hrdb.hr_html5_statistics.recom_view_num_pv</code>. 推荐浏览次数
     */
    public void setRecomViewNumPv(Integer value) {
        setValue(4, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.recom_view_num_pv</code>. 推荐浏览次数
     */
    public Integer getRecomViewNumPv() {
        return (Integer) getValue(4);
    }

    /**
     * Setter for <code>hrdb.hr_html5_statistics.create_date</code>. 创建日期
     */
    public void setCreateDate(Date value) {
        setValue(5, value);
    }

    /**
     * Getter for <code>hrdb.hr_html5_statistics.create_date</code>. 创建日期
     */
    public Date getCreateDate() {
        return (Date) getValue(5);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record6 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, Integer, Integer, Integer, Integer, Date> fieldsRow() {
        return (Row6) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row6<Integer, Integer, Integer, Integer, Integer, Date> valuesRow() {
        return (Row6) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field2() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.TOPIC_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field3() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.COMPANY_ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field4() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.VIEW_NUM_PV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field5() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.RECOM_VIEW_NUM_PV;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Date> field6() {
        return HrHtml5Statistics.HR_HTML5_STATISTICS.CREATE_DATE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value2() {
        return getTopicId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value3() {
        return getCompanyId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value4() {
        return getViewNumPv();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value5() {
        return getRecomViewNumPv();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date value6() {
        return getCreateDate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value2(Integer value) {
        setTopicId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value3(Integer value) {
        setCompanyId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value4(Integer value) {
        setViewNumPv(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value5(Integer value) {
        setRecomViewNumPv(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord value6(Date value) {
        setCreateDate(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrHtml5StatisticsRecord values(Integer value1, Integer value2, Integer value3, Integer value4, Integer value5, Date value6) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        value6(value6);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached HrHtml5StatisticsRecord
     */
    public HrHtml5StatisticsRecord() {
        super(HrHtml5Statistics.HR_HTML5_STATISTICS);
    }

    /**
     * Create a detached, initialised HrHtml5StatisticsRecord
     */
    public HrHtml5StatisticsRecord(Integer id, Integer topicId, Integer companyId, Integer viewNumPv, Integer recomViewNumPv, Date createDate) {
        super(HrHtml5Statistics.HR_HTML5_STATISTICS);

        setValue(0, id);
        setValue(1, topicId);
        setValue(2, companyId);
        setValue(3, viewNumPv);
        setValue(4, recomViewNumPv);
        setValue(5, createDate);
    }
}
