/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.dictdb.tables.records;


import com.y.tools.jooq.dictdb.tables.DictIndustry;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.TableRecordImpl;
import org.jooq.types.UInteger;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class DictIndustryRecord extends TableRecordImpl<DictIndustryRecord> implements Record3<UInteger, String, UInteger> {

    private static final long serialVersionUID = 1840187246;

    /**
     * Setter for <code>dictdb.dict_industry.code</code>. 字典code
     */
    public void setCode(UInteger value) {
        setValue(0, value);
    }

    /**
     * Getter for <code>dictdb.dict_industry.code</code>. 字典code
     */
    public UInteger getCode() {
        return (UInteger) getValue(0);
    }

    /**
     * Setter for <code>dictdb.dict_industry.name</code>. 字典name
     */
    public void setName(String value) {
        setValue(1, value);
    }

    /**
     * Getter for <code>dictdb.dict_industry.name</code>. 字典name
     */
    public String getName() {
        return (String) getValue(1);
    }

    /**
     * Setter for <code>dictdb.dict_industry.type</code>. 字典分类code
     */
    public void setType(UInteger value) {
        setValue(2, value);
    }

    /**
     * Getter for <code>dictdb.dict_industry.type</code>. 字典分类code
     */
    public UInteger getType() {
        return (UInteger) getValue(2);
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<UInteger, String, UInteger> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row3<UInteger, String, UInteger> valuesRow() {
        return (Row3) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field1() {
        return DictIndustry.DICT_INDUSTRY.CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return DictIndustry.DICT_INDUSTRY.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field3() {
        return DictIndustry.DICT_INDUSTRY.TYPE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value1() {
        return getCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value3() {
        return getType();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryRecord value1(UInteger value) {
        setCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryRecord value3(UInteger value) {
        setType(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryRecord values(UInteger value1, String value2, UInteger value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached DictIndustryRecord
     */
    public DictIndustryRecord() {
        super(DictIndustry.DICT_INDUSTRY);
    }

    /**
     * Create a detached, initialised DictIndustryRecord
     */
    public DictIndustryRecord(UInteger code, String name, UInteger type) {
        super(DictIndustry.DICT_INDUSTRY);

        setValue(0, code);
        setValue(1, name);
        setValue(2, type);
    }
}
