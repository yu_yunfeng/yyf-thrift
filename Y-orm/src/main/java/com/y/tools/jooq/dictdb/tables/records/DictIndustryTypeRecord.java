/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.dictdb.tables.records;


import com.y.tools.jooq.dictdb.tables.DictIndustryType;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record2;
import org.jooq.Row2;
import org.jooq.impl.TableRecordImpl;
import org.jooq.types.UInteger;


/**
 * 行业一级分类字典表
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class DictIndustryTypeRecord extends TableRecordImpl<DictIndustryTypeRecord> implements Record2<UInteger, String> {

    private static final long serialVersionUID = -777627775;

    /**
     * Setter for <code>dictdb.dict_industry_type.code</code>. 字典code
     */
    public void setCode(UInteger value) {
        setValue(0, value);
    }

    /**
     * Getter for <code>dictdb.dict_industry_type.code</code>. 字典code
     */
    public UInteger getCode() {
        return (UInteger) getValue(0);
    }

    /**
     * Setter for <code>dictdb.dict_industry_type.name</code>. 字典name
     */
    public void setName(String value) {
        setValue(1, value);
    }

    /**
     * Getter for <code>dictdb.dict_industry_type.name</code>. 字典name
     */
    public String getName() {
        return (String) getValue(1);
    }

    // -------------------------------------------------------------------------
    // Record2 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<UInteger, String> fieldsRow() {
        return (Row2) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row2<UInteger, String> valuesRow() {
        return (Row2) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field1() {
        return DictIndustryType.DICT_INDUSTRY_TYPE.CODE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field2() {
        return DictIndustryType.DICT_INDUSTRY_TYPE.NAME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value1() {
        return getCode();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value2() {
        return getName();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryTypeRecord value1(UInteger value) {
        setCode(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryTypeRecord value2(String value) {
        setName(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DictIndustryTypeRecord values(UInteger value1, String value2) {
        value1(value1);
        value2(value2);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached DictIndustryTypeRecord
     */
    public DictIndustryTypeRecord() {
        super(DictIndustryType.DICT_INDUSTRY_TYPE);
    }

    /**
     * Create a detached, initialised DictIndustryTypeRecord
     */
    public DictIndustryTypeRecord(UInteger code, String name) {
        super(DictIndustryType.DICT_INDUSTRY_TYPE);

        setValue(0, code);
        setValue(1, name);
    }
}
