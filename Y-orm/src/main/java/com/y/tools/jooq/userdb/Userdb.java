/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.userdb;


import com.y.tools.jooq.userdb.tables.CandidateVJobPositionRecom;
import com.y.tools.jooq.userdb.tables.UserBdUser;
import com.y.tools.jooq.userdb.tables.UserCompanyFollow;
import com.y.tools.jooq.userdb.tables.UserCompanyVisitReq;
import com.y.tools.jooq.userdb.tables.UserEmployee;
import com.y.tools.jooq.userdb.tables.UserEmployeePointsRecord;
import com.y.tools.jooq.userdb.tables.UserFavPosition;
import com.y.tools.jooq.userdb.tables.UserHrAccount;
import com.y.tools.jooq.userdb.tables.UserIntention;
import com.y.tools.jooq.userdb.tables.UserSettings;
import com.y.tools.jooq.userdb.tables.UserThirdUser;
import com.y.tools.jooq.userdb.tables.UserThirdpartyUser;
import com.y.tools.jooq.userdb.tables.UserUser;
import com.y.tools.jooq.userdb.tables.UserWxUser;
import com.y.tools.jooq.userdb.tables.UserWxViewer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Table;
import org.jooq.impl.SchemaImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class Userdb extends SchemaImpl {

    private static final long serialVersionUID = -1061245563;

    /**
     * The reference instance of <code>userdb</code>
     */
    public static final Userdb USERDB = new Userdb();

    /**
     * No further instances allowed
     */
    private Userdb() {
        super("userdb");
    }

    @Override
    public final List<Table<?>> getTables() {
        List result = new ArrayList();
        result.addAll(getTables0());
        return result;
    }

    private final List<Table<?>> getTables0() {
        return Arrays.<Table<?>>asList(
                CandidateVJobPositionRecom.CANDIDATE_V_JOB_POSITION_RECOM,
                UserBdUser.USER_BD_USER,
                UserCompanyFollow.USER_COMPANY_FOLLOW,
                UserCompanyVisitReq.USER_COMPANY_VISIT_REQ,
                UserEmployee.USER_EMPLOYEE,
                UserEmployeePointsRecord.USER_EMPLOYEE_POINTS_RECORD,
                UserFavPosition.USER_FAV_POSITION,
                UserHrAccount.USER_HR_ACCOUNT,
                UserIntention.USER_INTENTION,
                UserSettings.USER_SETTINGS,
                UserThirdpartyUser.USER_THIRDPARTY_USER,
                UserThirdUser.USER_THIRD_USER,
                UserUser.USER_USER,
                UserWxUser.USER_WX_USER,
                UserWxViewer.USER_WX_VIEWER);
    }
}
