/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.jobdb;


import com.y.tools.jooq.jobdb.tables.JobApplication;
import com.y.tools.jooq.jobdb.tables.JobApplicationConf;
import com.y.tools.jooq.jobdb.tables.JobApplicationOnline;
import com.y.tools.jooq.jobdb.tables.JobApplicationStatusBeisen;
import com.y.tools.jooq.jobdb.tables.JobCustom;
import com.y.tools.jooq.jobdb.tables.JobOccupation;
import com.y.tools.jooq.jobdb.tables.JobOccupationRel;
import com.y.tools.jooq.jobdb.tables.JobPosition;
import com.y.tools.jooq.jobdb.tables.JobPositionCity;
import com.y.tools.jooq.jobdb.tables.JobPositionExt;
import com.y.tools.jooq.jobdb.tables.JobPositionShareTplConf;
import com.y.tools.jooq.jobdb.tables.JobPositionTopic;
import com.y.tools.jooq.jobdb.tables.JobResumeOther;

import javax.annotation.Generated;


/**
 * Convenience access to all tables in jobdb
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class Tables {

    /**
     * The table jobdb.job_application
     */
    public static final JobApplication JOB_APPLICATION = JobApplication.JOB_APPLICATION;

    /**
     * 部门申请配置表
     */
    public static final JobApplicationConf JOB_APPLICATION_CONF = JobApplicationConf.JOB_APPLICATION_CONF;

    /**
     * The table jobdb.job_application_online
     */
    public static final JobApplicationOnline JOB_APPLICATION_ONLINE = JobApplicationOnline.JOB_APPLICATION_ONLINE;

    /**
     * 申请状态记录（ats北森）
     */
    public static final JobApplicationStatusBeisen JOB_APPLICATION_STATUS_BEISEN = JobApplicationStatusBeisen.JOB_APPLICATION_STATUS_BEISEN;

    /**
     * 职位自定义字段配置表
     */
    public static final JobCustom JOB_CUSTOM = JobCustom.JOB_CUSTOM;

    /**
     * 公司自定义职能表
     */
    public static final JobOccupation JOB_OCCUPATION = JobOccupation.JOB_OCCUPATION;

    /**
     * 职位与职能关系表
     */
    public static final JobOccupationRel JOB_OCCUPATION_REL = JobOccupationRel.JOB_OCCUPATION_REL;

    /**
     * The table jobdb.job_position
     */
    public static final JobPosition JOB_POSITION = JobPosition.JOB_POSITION;

    /**
     * The table jobdb.job_position_city
     */
    public static final JobPositionCity JOB_POSITION_CITY = JobPositionCity.JOB_POSITION_CITY;

    /**
     * 职位信息扩展表
     */
    public static final JobPositionExt JOB_POSITION_EXT = JobPositionExt.JOB_POSITION_EXT;

    /**
     * 职位分享描述配置模板
     */
    public static final JobPositionShareTplConf JOB_POSITION_SHARE_TPL_CONF = JobPositionShareTplConf.JOB_POSITION_SHARE_TPL_CONF;

    /**
     * 职位主题活动关系表
     */
    public static final JobPositionTopic JOB_POSITION_TOPIC = JobPositionTopic.JOB_POSITION_TOPIC;

    /**
     * 自定义简历副本记录表
     */
    public static final JobResumeOther JOB_RESUME_OTHER = JobResumeOther.JOB_RESUME_OTHER;
}
