/**
 * This class is generated by jOOQ
 */
package com.y.tools.jooq.hrdb.tables.records;


import com.y.tools.jooq.hrdb.tables.HrWxImageReply;

import java.sql.Timestamp;

import javax.annotation.Generated;

import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Row5;
import org.jooq.impl.UpdatableRecordImpl;
import org.jooq.types.UInteger;


/**
 * 微信图片回复
 */
@Generated(
        value = {
                "http://www.com.y.tools.orm.jooq.org",
                "jOOQ version:3.7.3"
        },
        comments = "This class is generated by jOOQ"
)
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class HrWxImageReplyRecord extends UpdatableRecordImpl<HrWxImageReplyRecord> implements Record5<Integer, UInteger, String, Timestamp, Timestamp> {

    private static final long serialVersionUID = -80500618;

    /**
     * Setter for <code>hrdb.hr_wx_image_reply.id</code>.
     */
    public void setId(Integer value) {
        setValue(0, value);
    }

    /**
     * Getter for <code>hrdb.hr_wx_image_reply.id</code>.
     */
    public Integer getId() {
        return (Integer) getValue(0);
    }

    /**
     * Setter for <code>hrdb.hr_wx_image_reply.rid</code>. wx_rule.id, 规则ID
     */
    public void setRid(UInteger value) {
        setValue(1, value);
    }

    /**
     * Getter for <code>hrdb.hr_wx_image_reply.rid</code>. wx_rule.id, 规则ID
     */
    public UInteger getRid() {
        return (UInteger) getValue(1);
    }

    /**
     * Setter for <code>hrdb.hr_wx_image_reply.image</code>. 回复图片的相对路径
     */
    public void setImage(String value) {
        setValue(2, value);
    }

    /**
     * Getter for <code>hrdb.hr_wx_image_reply.image</code>. 回复图片的相对路径
     */
    public String getImage() {
        return (String) getValue(2);
    }

    /**
     * Setter for <code>hrdb.hr_wx_image_reply.create_time</code>.
     */
    public void setCreateTime(Timestamp value) {
        setValue(3, value);
    }

    /**
     * Getter for <code>hrdb.hr_wx_image_reply.create_time</code>.
     */
    public Timestamp getCreateTime() {
        return (Timestamp) getValue(3);
    }

    /**
     * Setter for <code>hrdb.hr_wx_image_reply.update_time</code>.
     */
    public void setUpdateTime(Timestamp value) {
        setValue(4, value);
    }

    /**
     * Getter for <code>hrdb.hr_wx_image_reply.update_time</code>.
     */
    public Timestamp getUpdateTime() {
        return (Timestamp) getValue(4);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Record1<Integer> key() {
        return (Record1) super.key();
    }

    // -------------------------------------------------------------------------
    // Record5 type implementation
    // -------------------------------------------------------------------------

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, UInteger, String, Timestamp, Timestamp> fieldsRow() {
        return (Row5) super.fieldsRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Row5<Integer, UInteger, String, Timestamp, Timestamp> valuesRow() {
        return (Row5) super.valuesRow();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Integer> field1() {
        return HrWxImageReply.HR_WX_IMAGE_REPLY.ID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<UInteger> field2() {
        return HrWxImageReply.HR_WX_IMAGE_REPLY.RID;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<String> field3() {
        return HrWxImageReply.HR_WX_IMAGE_REPLY.IMAGE;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field4() {
        return HrWxImageReply.HR_WX_IMAGE_REPLY.CREATE_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Field<Timestamp> field5() {
        return HrWxImageReply.HR_WX_IMAGE_REPLY.UPDATE_TIME;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Integer value1() {
        return getId();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UInteger value2() {
        return getRid();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String value3() {
        return getImage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value4() {
        return getCreateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Timestamp value5() {
        return getUpdateTime();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord value1(Integer value) {
        setId(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord value2(UInteger value) {
        setRid(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord value3(String value) {
        setImage(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord value4(Timestamp value) {
        setCreateTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord value5(Timestamp value) {
        setUpdateTime(value);
        return this;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public HrWxImageReplyRecord values(Integer value1, UInteger value2, String value3, Timestamp value4, Timestamp value5) {
        value1(value1);
        value2(value2);
        value3(value3);
        value4(value4);
        value5(value5);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    /**
     * Create a detached HrWxImageReplyRecord
     */
    public HrWxImageReplyRecord() {
        super(HrWxImageReply.HR_WX_IMAGE_REPLY);
    }

    /**
     * Create a detached, initialised HrWxImageReplyRecord
     */
    public HrWxImageReplyRecord(Integer id, UInteger rid, String image, Timestamp createTime, Timestamp updateTime) {
        super(HrWxImageReply.HR_WX_IMAGE_REPLY);

        setValue(0, id);
        setValue(1, rid);
        setValue(2, image);
        setValue(3, createTime);
        setValue(4, updateTime);
    }
}
