package com.y.spring;

import org.springframework.beans.factory.xml.DefaultBeanDefinitionDocumentReader;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by YYF
 *
 * Date: 2017/7/24
 *
 * Project_name :Y-Tools
 */
@Configuration
@ComponentScan({"com.y.spring"})
public class AppConfig extends DefaultBeanDefinitionDocumentReader {

}
