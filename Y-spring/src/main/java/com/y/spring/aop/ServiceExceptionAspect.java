package com.y.spring.aop;

import com.xiaoleilu.hutool.date.DateUnit;
import com.xiaoleilu.hutool.date.DateUtil;


/**
 * Created by YYF
 *
 * Date: 2017/7/26
 *
 * Project_name :Y-Tools
 */
public class ServiceExceptionAspect {


    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) {

        String startDate = "2017-01-12";
        String endDate = "2017-01-13";
        System.out.println(DateUtil.between(DateUtil.parseDate(startDate), DateUtil.parseDate(endDate), DateUnit.DAY));


    }
}
