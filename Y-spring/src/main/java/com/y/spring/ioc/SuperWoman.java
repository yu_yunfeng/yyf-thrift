package com.y.spring.ioc;


import org.springframework.stereotype.Component;

/**
 * Created by YYF
 *
 * Date: 2017/7/24
 *
 * Project_name :Y-Tools
 */
@Component
public class SuperWoman implements Human {

    @Override
    public void eat() {

        System.out.println("SuperWoman eat something!");
    }
}
