package com.y.spring.ioc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * Created by YYF
 *
 * Date: 2017/7/24
 *
 * Project_name :Y-Tools
 */
@Component
public class Man implements Human {

    @Value("yuyunfeng")
    private String name;

    @Value("man")
    private String sex;


    @Override
    public void eat() {
        System.out.println("man eat something!");
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}
