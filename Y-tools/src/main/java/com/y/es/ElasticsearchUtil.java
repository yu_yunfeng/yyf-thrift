package com.y.es;

import com.alibaba.fastjson.JSONObject;

import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentFactory;

import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by YYF
 *
 * Date: 2017/8/22
 *
 * 用于elasticsearch 的增删改查
 *
 * Project_name :Y-Project
 */
public class ElasticsearchUtil {


    // ClusterName
    private final static String esClusterName = "my-application";
    // ES地址
    private final static String esConnection = "127.0.0.1";
    // ES端口
    private final static Integer esPort = 9300;


    /**
     * ES连接
     *
     * @return client
     *
     * 使用完以后，需要释放
     */
    public TransportClient init() {
        Settings settings = Settings.settingsBuilder().put("cluster.name", esClusterName)
                .build();
        TransportClient client = null;
        try {
            client = TransportClient.builder().settings(settings).build()
                    .addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(esConnection), esPort));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    /**
     * 查询ES
     *
     * @return
     */
    public Map getDate() {
        Map map = new HashMap();
        TransportClient client = init();
        try {
            GetResponse response = init().prepareGet("awards", "award", "7388").execute().actionGet();
            UpdateRequest updateRequest = new UpdateRequest();
            updateRequest.index("awards");
            updateRequest.type("award");
            updateRequest.id("7388");
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("test", 1);


            JSONObject jsonObjectTemp = new JSONObject();
            jsonObjectTemp.put("test", jsonObject);

            System.out.println(jsonObjectTemp.toJSONString());
            // 更新
            updateRequest.doc(XContentFactory.jsonBuilder()
                    .startObject()
                    .field("awards", jsonObjectTemp)
                    .endObject());
            client.update(updateRequest).get();
            System.out.println(response);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            client.close();
        }
        return map;
    }


    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) {
        ElasticsearchUtil elasticsearchUtil = new ElasticsearchUtil();
        elasticsearchUtil.getDate();
    }


}
