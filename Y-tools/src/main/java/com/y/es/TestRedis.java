package com.y.es;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by YYF
 *
 * Date: 2017/11/13
 *
 * Project_name :Y-Project
 */
public class TestRedis {


    public static int clientTotal = 5000;

    public static int threadTotal = 200;

    private static AtomicInteger count = new AtomicInteger(0);

    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) throws Exception {
        ExecutorService executorService = Executors.newCachedThreadPool();
        final Semaphore semaphore = new Semaphore(threadTotal);
        final CountDownLatch countDownLatch = new CountDownLatch(clientTotal);
        for (int i = 0; i < clientTotal; i++) {
            executorService.execute(() -> {
                try {
//                    semaphore.acquire();
                    update();
//                    semaphore.release();
                } catch (Exception e) {
                }
                countDownLatch.countDown();
            });
        }
        countDownLatch.await();
        executorService.shutdown();

        System.out.println(count);

        System.out.println(5.00 - 4.90);

        System.out.println(300 - 210);


        int x = 5;
        System.out.println(x--);
        System.out.println(--x);
    }


    public synchronized static void update() {
        count.addAndGet(1);
    }

}
