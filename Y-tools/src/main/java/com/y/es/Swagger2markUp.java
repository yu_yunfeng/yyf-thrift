package com.y.es;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;

import io.github.swagger2markup.Swagger2MarkupConverter;

/**
 * Created by YYF
 *
 * Date: 2018/1/2
 *
 * Project_name :Y-Project
 */
public class Swagger2markUp {

    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) {
        try {
            Path localSwaggerFile = Paths.get("/Users/yuyunfeng/go/src/zhproject/swagger/swagger.yml");
            Path outputDirectory = Paths.get("/Users/yuyunfeng/Documents/workspace-fuxi/swagger-3");
//            Swagger2MarkupConverter.from(localSwaggerFile)
//                    .build()
//                    .toFolder(outputDirectory);

            String documentation = Swagger2MarkupConverter.from(localSwaggerFile)
                    .build()
                    .toString();

            System.out.println(documentation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
