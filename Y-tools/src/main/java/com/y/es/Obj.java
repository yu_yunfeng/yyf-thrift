package com.y.es;

/**
 * Created by YYF
 *
 * Date: 2017/8/22
 *
 * Project_name :Y-Project
 */
public class Obj {
    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
