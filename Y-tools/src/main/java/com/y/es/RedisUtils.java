package com.y.es;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
 * Created by YYF
 *
 * Date: 2017/11/13
 *
 * Project_name :Y-Project
 */
public class RedisUtils {

    // 地址
    private static String ADDR = "127.0.0.1";

    // 端口
    private static int PORT = 6379;

    // 密码
    private static String PASSWORD = "";

    // 最大连接数
    private static int MAX_ACTIVE = 1024;

    // 控制一个pool 最多有多少状态为空闲的
    private static int MAX_IDLE = 200;

    // 最大等待时间
    private static int MAX_WAIT = 10000;

    private static int TIMEOUT = 10000;

    // 在borrow一个jedis实例时，是否提前进行validate操作；如果为true，则得到的jedis实例均是可用的；
    private static boolean TEST_ON_BORROW = true;

    // redis 连接
    private static JedisPool jedisPool = null;


    static {
        try {
            JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
            jedisPoolConfig.setMaxIdle(MAX_IDLE);
            jedisPoolConfig.setMaxWaitMillis(MAX_WAIT);
            jedisPoolConfig.setTestOnBorrow(TEST_ON_BORROW);
            jedisPool = new JedisPool(jedisPoolConfig, ADDR, PORT, TIMEOUT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 获取Redis
     *
     * @return
     */
    public synchronized static Jedis getRedis() {
        try {
            if (jedisPool != null) {
                Jedis jedis = jedisPool.getResource();
                return jedis;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * 释放redis
     *
     * @param jedis
     */
    public static void returnResouce(Jedis jedis) {
        try {
            if (jedis != null) {
                jedisPool.returnBrokenResource(jedis);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
