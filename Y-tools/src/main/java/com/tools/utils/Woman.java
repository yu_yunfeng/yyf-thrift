package com.tools.utils;

/**
 * Created by YYF
 *
 * Date: 2018/5/2
 *
 * Project_name :Y-Project
 */
public class Woman implements Cloneable{

    private String hob;

    private People people;

    public String getHob() {
        return hob;
    }

    public void setHob(String hob) {
        this.hob = hob;
    }

    public People getPeople() {
        return people;
    }

    public void setPeople(People people) {
        this.people = people;
    }

    public Woman(String hob, People people) {
        this.hob = hob;
        this.people = people;
    }

    public Woman() {
    }

    @Override
    public String toString() {
        return "Woman{" +
                "hob='" + hob + '\'' +
                ", people=" + people +
                '}';
    }

    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

}
