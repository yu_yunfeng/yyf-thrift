package com.y.basic.designmode.singleton;

/**
 * Created by YYF
 *
 * Date: 2017/7/24
 *
 * 单例模式的实现
 *
 * 懒汉模式，线程不安全
 *
 * Project_name :Y-Tools
 */
public class SingletonObj {


    private static SingletonObj singletonObj;

    public SingletonObj() {

    }

    public static SingletonObj getInstance() {
        if (singletonObj == null) {
            singletonObj = new SingletonObj();
        }
        return singletonObj;
    }


}
