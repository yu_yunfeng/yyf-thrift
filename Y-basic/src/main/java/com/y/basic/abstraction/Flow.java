package com.y.basic.abstraction;

/**
 * Created by YYF
 *
 * Date: 2017/7/21
 *
 * Project_name :Y-Tools
 */
public abstract class Flow {


    public void start() {
        init();
    }

    abstract void init();
}
