package com.y.basic;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.File;
import java.io.IOException;
import java.util.BitSet;

/**
 * Created by YYF
 *
 * Date: 2017/8/3
 *
 * Project_name :Y-Tools
 */
public class Main {

    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) throws IOException {

        String a1 = "1";
        String b1 = "10";
        String c1 = "101";
        String d1 = "1101";

        String f1 = "1111";


        // 第一位代表 智联  第二位代表 前程 第三位代表 其他
//        System.out.println("智联: " + Integer.parseInt("001", 2));
//
//        System.out.println("前程: " + Integer.parseInt("010", 2));

//        System.out.println("其他: " + Integer.parseInt("1000000000", 2));

        BitSet bitSet = new BitSet();

        bitSet.set(3);

//        System.out.println(bitSet.get(0));


//        for (int i = bitSet.nextSetBit(0); i >= 0; i = bitSet.nextSetBit(i + 1)) {
//
//            System.out.print(i+"\t");
//        }

//        // 智联和前程
        System.out.println(Integer.toBinaryString((576)));

//        System.out.println(Integer.toBinaryString(3 | 2));

//        // 智联和其他
//        System.out.println(Integer.toBinaryString((1 | 4)));
//
//        // 前程和其他
//        System.out.println(Integer.toBinaryString((2 | 4)));


        Main.Test();
    }

    public static void Test() throws IOException {

        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost httpPost = new HttpPost("http://localhost:8899/profile/parser");
        File file = new File("/Users/yuyunfeng/Documents/YYF/yyf.docx");
        HttpEntity reqEntity = MultipartEntityBuilder.create().addPart("file", new FileBody(file)).build();
        httpPost.setEntity(reqEntity);
        HttpResponse response = httpclient.execute(httpPost);


//        HttpClient httpClient = HttpClientBuilder.create().build();
//
//        httpPost.setEntity(new StringEntity(data, Consts.UTF_8));
//        HttpEntity reqEntity = MultipartEntityBuilder.create().addPart("file", new FileBody(file));
//
//        HttpResponse httpResponse = httpClient.execute(httpPost);
//        HttpEntity httpEntity = httpResponse.getEntity();
//
//        // 设置头字段
//        httpPost.addHeader("content-type", "multipart/form-data");
//        JSONObject json = new JSONObject();
//        httpPost.setEntity(params);
//
//        // 发送请求
//        HttpClient httpclient = HttpClientBuilder.create().build();
//        HttpResponse response = httpclient.execute(httpPost);
//
//        // 处理返回结果
//        String resCont = EntityUtils.toString(response.getEntity(), Consts.UTF_8);
//        JSONObject res = JSON.parseObject(resCont);
//        System.out.println(res);

    }
}
