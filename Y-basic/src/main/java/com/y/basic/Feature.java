package com.y.basic;


/**
 * Created by YYF
 *
 * Date: 2017/8/3
 *
 * Project_name :Y-Tools
 */

public enum Feature {
    /**
     *
     */
    AutoCloseSource,
    /**
     *
     */
    AllowComment,
    /**
     *
     */
    AllowUnQuotedFieldNames,
    /**
     *
     */
    AllowSingleQuotes,
    /**
     *
     */
    InternFieldNames,
    /**
     *
     */
    AllowISO8601DateFormat,

    /**
     * {"a":1,,,"b":2}
     */
    AllowArbitraryCommas,

    /**
     *
     */
    UseBigDecimal,

    /**
     * @since 1.1.2
     */
    IgnoreNotMatch,

    /**
     * @since 1.1.3
     */
    SortFeidFastMatch,

    /**
     * @since 1.1.3
     */
    DisableASM,

    /**
     * @since 1.1.7
     */
    DisableCircularReferenceDetect,

    /**
     * @since 1.1.10
     */
    InitStringFieldAsEmpty,

    /**
     * @since 1.1.35
     */
    SupportArrayToBean,

    /**
     * @since 1.2.3
     */
    OrderedField,

    /**
     * @since 1.2.5
     */
    DisableSpecialKeyDetect,

    /**
     * @since 1.2.9
     */
    UseObjectArray,

    /**
     * @since 1.2.22, 1.1.54.android
     */
    SupportNonPublicField;

    Feature() {
        mask = (1 << ordinal());
    }

    public final int mask;

    public final int getMask() {
        return mask;
    }

    public static boolean isEnabled(int features, com.alibaba.fastjson.parser.Feature feature) {
        return (features & feature.mask) != 0;
    }

    public static int config(int features, com.alibaba.fastjson.parser.Feature feature, boolean state) {
        if (state) {
            features |= feature.mask;
        } else {
            features &= ~feature.mask;
        }

        return features;
    }

    public static int of(com.alibaba.fastjson.parser.Feature[] features) {
        if (features == null) {
            return 0;
        }

        int value = 0;

        for (com.alibaba.fastjson.parser.Feature feature : features) {
            value |= feature.mask;
        }

        return value;
    }
}
