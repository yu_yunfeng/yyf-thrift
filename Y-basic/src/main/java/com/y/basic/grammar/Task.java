package com.y.basic.grammar;

/**
 * Created by YYF
 *
 * Date: 2017/7/21
 *
 * Project_name :Y-Tools
 */
public interface Task {

    void handler();
}
