package com.y.basic.jdbc;

import com.xiaoleilu.hutool.db.ds.DSFactory;
import com.xiaoleilu.hutool.setting.Setting;

import javax.sql.DataSource;

/**
 * Created by YYF
 *
 * Date: 2017/7/27
 *
 * Project_name :Y-Tools
 */
public class DDPoolFactory extends DSFactory {

    public DDPoolFactory(String dataSourceName, Class<? extends DataSource> dataSourceClass, Setting setting) {
        super(dataSourceName, dataSourceClass, setting);
    }

    @Override
    public DataSource getDataSource(String s) {
        return null;
    }

    @Override
    public void close(String s) {

    }

    @Override
    public void destroy() {

    }
}
