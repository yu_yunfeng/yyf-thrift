package com.y.basic.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * Created by YYF
 *
 * Date: 2017/6/23
 *
 * Project_name :Y-Tools
 */
public class JDBCConnection {

    public static final String DBDRIVER = "com.mysql.jdbc.Driver";

    public static final String DBURL = "jdbc:mysql://db-t.dqprism.com:3306/jobdb?useUnicode=true&characterEncoding=utf8&autoReconnect=true&rewriteBatchedStatements=TRUE&zeroDateTimeBehavior=convertToNull";

    public static final String DBUSER = "www";

    public static final String DBPASSWORD = "5F51692091B4031640E18E7C27430E071BC878C8";


    public static Connection getConnection() {
        Connection conn = null;
        try {
            Class.forName(DBDRIVER);
            conn = DriverManager.getConnection(DBURL, DBUSER, DBPASSWORD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return conn;
    }
}
