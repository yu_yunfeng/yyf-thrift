package com.y.basic.jdbc;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by YYF
 *
 * Date: 2017/6/23
 *
 * Project_name :Y-Tools
 */
public class JDBCTest {

    public static void main(String args[]) throws SQLException {

        Connection connection = JDBCConnection.getConnection();
        try {
            String sql = "SELECT * FROM job_position WHERE company_id=39978";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            ResultSetMetaData set = resultSet.getMetaData();
            Map<String, Object> map = new HashMap();
            while (resultSet.next()) {
                for (int i = 1; i <= set.getColumnCount(); i++) {
                    map.put(set.getColumnLabel(i), resultSet.getObject(i));
                }
            }
            System.out.println(map.toString());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            connection.close();
        }

    }
}
