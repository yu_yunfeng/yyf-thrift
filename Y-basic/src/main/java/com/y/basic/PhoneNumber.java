package com.y.basic;

/**
 * Created by YYF
 *
 * Date: 2017/8/7
 *
 * Project_name :Y-Tools
 */
public final class PhoneNumber {

    private Integer areaCode;
    private Integer prefix;
    private Integer lineNumber;

    public Integer getAreaCode() {
        return areaCode;
    }

    public void setAreaCode(Integer areaCode) {
        this.areaCode = areaCode;
    }

    public Integer getPrefix() {
        return prefix;
    }

    public void setPrefix(Integer prefix) {
        this.prefix = prefix;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }
}
