package com.y.hadoop;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FSDataOutputStream;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

import java.net.URI;

/**
 * Created by YYF
 *
 * Date: 2017/8/14
 *
 * Project_name :Y-Project
 */
public class HDFSEntity {
    /**
     * 主入口
     *
     * @param args
     */
    public static void main(String args[]) {
        try {
            Configuration conf = new Configuration();
            conf.set("fs.defaultFS", "hdfs://hadoop:9000");
            FileSystem fileSystem = FileSystem.get(new URI("hdfs://hadoop:9000"), conf, "hadoop");

            byte[] buff = "hello hadoop world!\n".getBytes();

            Path dfs = new Path("/demo");
            FSDataOutputStream outputStream = fileSystem.create(dfs);
//            outputStream.write(buff, 0, buff.length);

            System.out.println(fileSystem.listStatus(new Path("/")));
            System.out.println(fileSystem.getHomeDirectory());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
